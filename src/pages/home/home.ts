import { ListPage } from './../list/list';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  result: any;
  keyword: string;

  constructor(public navCtrl: NavController, private toast: ToastController, private http: Http) {
    
  }
  onPushList() {
    this.navCtrl.push(ListPage, {title: 'From Home Page'});
  }
  onSetrootList() {
    this.navCtrl.setRoot(ListPage);
  }
  onPushAbout() {
    this.navCtrl.push('AboutPage');
  }
  onToast() {
    this.toast.create(
      {message: "Hello toast", 
      duration: 2000}
    ).present()
  }
  onFab() {

  }
  onSearch() {
    this.http.get("http://www.omdbapi.com/?t="+this.keyword+"&apikey=1bcec5ea")
    .map(res=>res.json())
    .subscribe( data=>
      { console.log(data); 		
        this.result = data;		
      }
    ); 
  }
}
